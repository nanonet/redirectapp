const express = require('express');
const app = express();
const config = require('./config');

app.get('/', (req, res) => {
    res.redirect(config.redirect);
});

app.listen(config.port, () => {
    console.log(`Example app listening on port ${config.port}!`);
});